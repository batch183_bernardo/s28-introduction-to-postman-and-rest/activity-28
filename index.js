

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
	let list = json.map(todos => console.log(todos.title));
});

// GET METHOD

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json))

//PRINT MESSAGE

console.log(`The item "delectus au autem" on the list has a status of false`);

//POST METHOD

fetch("https://jsonplaceholder.typicode.com/todos", {

	method: "POST",

	headers:{
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
			userId: 1,
			title: "Created To Do List Item",
			completed: false
	})

})
.then((response) => response.json())
.then((json) => console.log(json))

// PUT METHOD

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
			userId: 1,
			title: "Updated To Do List Item",
			completed: false,
			dateCompleted: "Pending",
			status: "Pending"

		})
})
.then((response) => response.json())
.then((json) => console.log(json))

// PATCH METHOD

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete"
		})
})
.then((response) => response.json())
.then((json) => console.log(json))

//DELETE METHOD

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})